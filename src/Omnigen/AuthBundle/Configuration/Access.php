<?php


namespace Omnigen\AuthBundle\Configuration;


/**
 * The annotation which requires a valid access token to be available
 *
 * @Annotation
 */
class Access
{
    /**
     * Valid values: any, user, client
     *
     * @var string
     */
    public $type = 'all';
}
