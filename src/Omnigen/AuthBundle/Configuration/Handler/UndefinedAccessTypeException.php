<?php


namespace Omnigen\AuthBundle\Configuration\Handler;


use Throwable;

class UndefinedAccessTypeException extends \RuntimeException
{
    public function __construct(string $type, $code = 0, Throwable $previous = null)
    {
        parent::__construct('Unrecognised access type: ' . $type, $code, $previous);
    }
}
