<?php


namespace Omnigen\AuthBundle\Configuration\Handler;


use Omnigen\Auth\Structs\BaseUser;

class AllAccessHandler implements AccessHandler
{
    /**
     * @inheritDoc
     */
    public function handle(BaseUser $baseUser): bool
    {
        return true;
    }
}
