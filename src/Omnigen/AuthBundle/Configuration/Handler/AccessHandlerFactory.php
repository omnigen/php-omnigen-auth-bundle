<?php


namespace Omnigen\AuthBundle\Configuration\Handler;


use Omnigen\Auth\Structs\BaseUser;
use Omnigen\AuthBundle\Configuration\Access;

class AccessHandlerFactory
{
    private static $map = [];

    public static function check(Access $access, BaseUser $baseUser): bool
    {
        $type = strtolower($access->type);
        $handler = self::getHandler($type);
        if (!$handler) {
            throw new UndefinedAccessTypeException($access->type);
        }
        return $handler->handle($baseUser);
    }

    private static function getHandler(string $type)
    {
        if (empty(self::$map)) {
            self::$map = [
                'all' => new AllAccessHandler(),
                'valid' => new ValidAccessHandler(),
                'user' => new UserAccessHandler(),
                'client' => new ClientAccessHandler()
            ];
        }

        if (isset(self::$map[$type])) {
            return self::$map[$type];
        }
        return null;
    }
}
