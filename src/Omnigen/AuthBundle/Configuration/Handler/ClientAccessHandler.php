<?php


namespace Omnigen\AuthBundle\Configuration\Handler;


use Omnigen\Auth\Structs\BaseUser;

class ClientAccessHandler implements AccessHandler
{
    /**
     * @inheritDoc
     */
    public function handle(BaseUser $baseUser): bool
    {
        return $baseUser->isClientLoggedIn();
    }
}
