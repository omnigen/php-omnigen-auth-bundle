<?php


namespace Omnigen\AuthBundle\Configuration\Handler;


use Omnigen\Auth\Structs\BaseUser;

interface AccessHandler
{
    /**
     * Checks whether the user object is valid according to this handler
     *
     * @param BaseUser $baseUser
     * @return bool Whether the user is valid
     */
    public function handle(BaseUser $baseUser): bool;
}
