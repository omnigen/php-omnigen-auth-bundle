<?php


namespace Omnigen\AuthBundle\Configuration\Handler;


use Omnigen\Auth\Structs\BaseUser;

class ValidAccessHandler implements AccessHandler
{
    /**
     * @inheritDoc
     */
    public function handle(BaseUser $baseUser): bool
    {
        return $baseUser->isValid();
    }
}
