<?php


namespace Omnigen\AuthBundle\Configuration\Handler;


use Omnigen\Auth\Structs\BaseUser;

class UserAccessHandler implements AccessHandler
{
    /**
     * @inheritDoc
     */
    public function handle(BaseUser $baseUser): bool
    {
        return $baseUser->isUserLoggedIn();
    }
}
