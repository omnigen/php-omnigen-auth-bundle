<?php


namespace Omnigen\AuthBundle\Service;


use Omnigen\Auth\AccessTokenValidator;
use Omnigen\Auth\AuthApi;
use Omnigen\Auth\Structs\BaseUser;
use Omnigen\Auth\Utils;
use Omnigen\AuthBundle\Exceptions\InvalidConfigurationException;
use Symfony\Component\HttpFoundation\Request;

class OmnigenAuthService
{
    const AUTHORIZATION_HEADER = 'Authorization';
    const ACCESS_TOKEN_TYPE = 'Bearer';

    /**
     * @var AccessTokenValidator
     */
    private $accessTokenValidator;

    /**
     * @param string $domain
     * @param string|null $consumerId
     * @param string|null $consumerSecret
     */
    public function setAccessTokenValidatorCreds(string $domain, $consumerId, $consumerSecret)
    {
        if (empty($domain)) {
            throw new InvalidConfigurationException('No auth domain provided');
        }

        if (empty($consumerId) || empty($consumerSecret)) {
            throw new InvalidConfigurationException('No consumer credentials supplied');
        }

        if (!filter_var($domain, FILTER_VALIDATE_URL)) {
            throw new InvalidConfigurationException('Auth domain is not a valid URL');
        }

        $this->accessTokenValidator = new AccessTokenValidator($domain, $consumerId, $consumerSecret, true, true);
    }

    /**
     * @return \Omnigen\Auth\AuthApi
     */
    public function getAuthApi(): AuthApi
    {
        return $this->accessTokenValidator->getApiInterface();
    }

    /**
     * @param string $accessToken
     * @return \Omnigen\Auth\Structs\BaseUser|null
     */
    public function getUserData(string $accessToken): ?BaseUser
    {
        if ($accessToken === null)
            return null;
        return $this->accessTokenValidator->getAccessTokenData($accessToken);
    }

    /**
     * @param Request $request
     * @return \Omnigen\Auth\Structs\BaseUser|null
     */
    public function getUserDataFromRequest(Request $request): ?BaseUser
    {
        $accessToken = $this->fetchAccessTokenFromRequest($request);
        if ($accessToken === null)
            return $accessToken;
        return $this->getUserData($accessToken);
    }

    /**
     * @param Request $request
     * @return string|null
     */
    public function fetchAccessTokenFromRequest(Request $request): ?string
    {
        if (!$request->headers->has(self::AUTHORIZATION_HEADER)) return null;
        $authHeader = $request->headers->get(self::AUTHORIZATION_HEADER);
        if (!Utils::stringStartsWith($authHeader, self::ACCESS_TOKEN_TYPE)) return null;

        return trim(substr($authHeader, strlen(self::ACCESS_TOKEN_TYPE)));
    }
}
