<?php


namespace Omnigen\AuthBundle\Service;


use Omnigen\Auth\Structs\BaseUser;

class OmnigenUserService
{
    /**
     * @var BaseUser
     */
    private $userData;

    public function setUserData(BaseUser $user)
    {
        $this->userData = $user;
    }

    /**
     * Retrieves the currently authenticated user object (which could also be a client). When the Access annotation
     * has been used, this has already been validated.
     *
     * @return BaseUser
     */
    public function getUser(): BaseUser
    {
        return $this->getUser();
    }
}
