<?php


namespace Omnigen\AuthBundle\DependencyInjection;


use Omnigen\AuthBundle\EventListener\AccessEventListener;
use Omnigen\AuthBundle\Exceptions\InvalidConfigurationException;
use Omnigen\AuthBundle\Service\OmnigenUserService;
use Symfony\Component\Config\FileLocator;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Extension\Extension;
use Symfony\Component\DependencyInjection\Loader\XmlFileLoader;
use Symfony\Component\DependencyInjection\Reference;

class OmnigenAuthExtension extends Extension
{
    /**
     * @inheritDoc
     */
    public function load(array $configs, ContainerBuilder $container)
    {
        // Load services of this bundle
        $loader = new XmlFileLoader($container, new FileLocator(__DIR__ . '/../Resources/config'));
        $loader->load('services.xml');

        // Load user defined configuration
        $processor = new ConfigurationProcessor();
        $configData = $this->processConfiguration($processor, $configs);

        $container->resolveEnvPlaceholders($configData);

        $container->setParameter('omnigen_auth.domain', $configData['domain']);
        $container->setParameter('omnigen_auth.consumer_id', $configData['consumer_id']);
        $container->setParameter('omnigen_auth.consumer_secret', $configData['consumer_secret']);

        // Load the annotation handler
        if (!$configData['disable_annotation']) {
            // Register the user service
            $container
                ->register('omnigen_auth.user_storage', OmnigenUserService::class)
                ->setPublic(true)->setAutowired(true);

            // Register the event listener
            $container
                ->register('omnigen_auth.access_listener', AccessEventListener::class)
                ->setArgument(0, new Reference('annotation_reader'))
                ->setArgument(1, new Reference('omnigen_auth.authenticate'))
                ->setArgument(2, new Reference('omnigen_auth.user_storage'))
                ->addTag('kernel.event_subscriber')
                ->setPublic(false);
        }

        $accessListenerDefinition = $container->getDefinition('omnigen_auth.authenticate');
        $accessListenerDefinition->addMethodCall(
            'setAccessTokenValidatorCreds', array(
            $configData['domain'], $configData['consumer_id'], $configData['consumer_secret']));
    }
}
