<?php


namespace Omnigen\AuthBundle\DependencyInjection;


use Symfony\Component\Config\Definition\Builder\TreeBuilder;
use Symfony\Component\Config\Definition\ConfigurationInterface;

class ConfigurationProcessor implements ConfigurationInterface
{
    /**
     * @inheritDoc
     */
    public function getConfigTreeBuilder()
    {
        $treeBuilder = new TreeBuilder('omnigen_auth');

        $treeBuilder->getRootNode()
            ->children()
                ->scalarNode('domain')
                    ->info('The fully qualified URL to the domain where Omnigen\'s authorization server is hosted')
                    ->isRequired()
                    ->cannotBeEmpty()
                ->end()
                ->scalarNode('consumer_id')
                    ->defaultNull()
                    ->cannotBeEmpty()
                ->end()
                ->scalarNode('consumer_secret')
                    ->defaultNull()
                    ->cannotBeEmpty()
                ->end()
                ->booleanNode('disable_annotation')
                    ->defaultFalse()
                ->end()
            ->end();
        return $treeBuilder;
    }
}
