<?php


namespace Omnigen\AuthBundle\EventListener;


use Doctrine\Common\Annotations\Reader;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpKernel\Event\FilterControllerEvent;

abstract class AbstractAnnotationHandler implements EventSubscriberInterface
{
    const FILTER_EVENT_HANDLER = 'handleFilterEvent';

    /**
     * @var Reader
     */
    protected $annotationReader;

    protected $annotationClass;

    public function __construct(Reader $reader, $annotationClass)
    {
        $this->annotationReader = $reader;
        $this->annotationClass = $annotationClass;
    }

    /**
     * Method to point a controller event towards
     *
     * @param FilterControllerEvent $event
     * @return mixed
     * @throws \ReflectionException
     */
    public function handleFilterEvent(FilterControllerEvent $event)
    {
        if (!is_array($controllers = $event->getController())) {
            return;
        }
        // Retrieve the annotations from the controller class and controller method
        // If the annotation is available on the controller method, this overrides the controller class annotation
        list($controller, $methodName) = $controllers;

        $classAnnotation = $this->getClassAnnotation($controller, $this->annotationClass);
        $methodAnnotation = $this->getMethodAnnotation($controller, $methodName, $this->annotationClass);
        return $this->handleAnnotation($event, $classAnnotation, $methodAnnotation);
    }

    /**
     * Method which handles the annotation
     *
     * @param FilterControllerEvent $event
     * @param $classAnnotation
     * @param $methodAnnotation
     * @return mixed
     */
    public abstract function handleAnnotation(FilterControllerEvent $event, $classAnnotation, $methodAnnotation);

    /**
     * @param $controller
     * @param $annotationClass
     * @return object|null
     * @throws \ReflectionException
     */
    protected function getClassAnnotation($controller, $annotationClass)
    {
        $reflectionClass = new \ReflectionClass($controller);
        return $this->annotationReader->getClassAnnotation($reflectionClass, $annotationClass);
    }

    /**
     * @param $controller
     * @param string $method
     * @param $annotationClass
     * @return object|null
     * @throws \ReflectionException
     */
    protected function getMethodAnnotation($controller, string $method, $annotationClass)
    {
        $reflectionObject = new \ReflectionObject($controller);
        $reflectionMethod = $reflectionObject->getMethod($method);
        return $this->annotationReader->getMethodAnnotation($reflectionMethod, $annotationClass);
    }
}
