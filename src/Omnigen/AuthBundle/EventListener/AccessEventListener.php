<?php


namespace Omnigen\AuthBundle\EventListener;


use Doctrine\Common\Annotations\Reader;
use Omnigen\AuthBundle\Configuration\Access;
use Omnigen\AuthBundle\Configuration\Handler\AccessHandlerFactory;
use Omnigen\AuthBundle\Exceptions\InvalidConfigurationException;
use Omnigen\AuthBundle\Service\OmnigenAuthService;
use Omnigen\AuthBundle\Service\OmnigenUserService;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Event\FilterControllerEvent;
use Symfony\Component\HttpKernel\KernelEvents;

class AccessEventListener extends AbstractAnnotationHandler
{
    const ALLOWED_TYPES = ['all', 'valid', 'user', 'client'];

    /**
     * @var OmnigenAuthService
     */
    private $authService;

    /**
     * @var OmnigenUserService
     */
    private $userService;

    /**
     * AccessEventListener constructor.
     * @param Reader $reader
     * @param OmnigenUserService $userService
     */
    public function __construct(Reader $reader, OmnigenAuthService $authService, OmnigenUserService $userService)
    {
        parent::__construct($reader, Access::class);
        $this->authService = $authService;
        $this->userService = $userService;
    }

    /**
     * {@inheritdoc}
     */
    public static function getSubscribedEvents()
    {
        return [KernelEvents::CONTROLLER_ARGUMENTS => self::FILTER_EVENT_HANDLER];
    }

    /**
     * @inheritDoc
     */
    public function handleAnnotation(FilterControllerEvent $event, $classAnnotation, $methodAnnotation)
    {
        $handleAnnotation = $methodAnnotation === null ? $classAnnotation : $methodAnnotation;
        if ($handleAnnotation == null) {
            // No annotations to handle
            return;
        }

        if (!in_array($handleAnnotation->type, self::ALLOWED_TYPES)) {
            throw new InvalidConfigurationException($handleAnnotation->type . ' not allowed for @Access');
        }
        if ($handleAnnotation->type === 'any') {
            // No authorization required
            return;
        }

        // Check if the authorization header is available
        $request = $event->getRequest();
        $userData = $this->authService->getUserDataFromRequest($request);
        if ($userData === null) {
            $event->setController(self::getForbiddenResponse());
            return;
        }

        if (!AccessHandlerFactory::check($handleAnnotation, $userData)) {
            $event->setController(self::getForbiddenResponse());
            return;
        }

        $this->userService->setUserData($userData);
    }

    /**
     * @return \Closure
     */
    public static function getForbiddenResponse()
    {
        return function () {
            $response = new Response();
            $response->setStatusCode(Response::HTTP_FORBIDDEN);
            return $response;
        };
    }
}
