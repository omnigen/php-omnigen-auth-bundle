<?php


namespace App\Controller;

use Omnigen\AuthBundle\Configuration\Access;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class TestController
 * @package App\Controller
 * @Route("/api/")
 */
class TestController extends AbstractController
{
    /**
     * @Route("", name="test-restricted", methods={"GET"})
     * @Access(type="user")
     */
    public function testRestricted()
    {
        return $this->json(["endpoint" => "test-restricted", "restricted" => "true"]);
    }

    /**
     * @Route("unrestricted", name="test-unrestricted", methods={"GET"})
     */
    public function testUnrestricted()
    {
        return $this->json(["endpoint" => "test-unrestricted", "restricted" => "false"]);
    }
}
