<?php


namespace App\Controller;


use Omnigen\AuthBundle\Configuration\Access;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;


/**
 * Class RestrictedController
 * @package App\Controller
 * @Route("/api/user-restricted")
 * @Access(type="user")
 */
class RestrictedController extends AbstractController
{

    /**
     * @Route("", name="test", methods="GET")
     * @return \Symfony\Component\HttpFoundation\JsonResponse
     */
    public function test()
    {
        return $this->json(["endpoint" => "test-restricted", "restricted" => "true"]);
    }

    /**
     * @Route("/client", name="client-restricted", methods="GET")
     * @Access(type="client")
     * @return \Symfony\Component\HttpFoundation\JsonResponse
     */
    public function override()
    {
        return $this->json(["endpoint" => "client restricted", "restricted" => "client"]);
    }
}
