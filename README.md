# php-omnigen-auth-bundle

This bundle provides services to retrieve data from an access token which then can be used by the implementing party to either validate authentication, and get data about the authenticated user from the authorization service.

To make this process easier, an annotation has been implemented which can be used either on complete controllers or specific routes in those controllers. Note that this annotation first looks for the annotation on the router method, and if the annotation is not present then looks at the class. When no annotation is found on both places, the endpoints are assumed to be public.

This `@Access` annotation requires an argument, `type`, which can be one of the following three options:

1. `all`: Which is the same as omitting this annotation. This can be used however to override the annotation defined on the controller class.
1. `valid`: The access token has to be valid. The type of data which is bound to this access token does not matter.
1. `user`: The access token has to be valid and be of type user
1. `client`: The access token has to be valid an be of type client

When one wants to access the user data from a route protected by the `@Access` annotation, the `OmnigenUserService` can be used. The `OmnigenAuthService` is responsible for validating the access tokens and retrieving the associated user data. This service can also be used to retrieve the underlying `AuthApi` object from the [php-omnigen-auth](https://bitbucket.org/omnigen/php-omnigen-auth) library.

## Configuration

This bundle is configured through symfony's default configuration system. The available configuration options are:

* `domain`: An URL pointing to the authorization server. **required**
* `consumer_id`: The consumer identifier used for authorization to the auth server. Used in combination with the `consumer_secret`.
* `consumer_secret`: The consumer secret used for authorization to the auth server. Used in combination with the `consumer_id`.
* `is_app_engine`: Whether authorization to the auth server is handled based on app engine identifiers. **default**: false
* `disable_annotation`: Whether to disable the event handler for the `@Access` annotation, which introduces slight overhead. It is recommended to set this to true when the annotation is not used. **default**: false
